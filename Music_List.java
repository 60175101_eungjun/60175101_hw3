package com.example.mp3_app;

import java.io.Serializable;

public class Music_List implements Serializable {
    private int album;
    private String title;
    private String artist;
    public int getAlbum() {
        return album;
    }
    public void setAlbum(int album) {
        this.album = album;
    }
    public String getTitle() {
        return title;
    }
    public void setTitle(String title) {
        this.title = title;
    }
    public String getArtist() {
        return artist;
    }
    public void setArtist(String artist) {
        this.artist = artist;
    }
}