# hw3
MP3 플레이어를 구현한다.
========================

<pre>
※ h2때부터 push가 거부되는 문제가 있어서 프로그램 전체를 push하지는 못했습니다.※

Main UI : 음악 재생 목록 화면
Music Play UI : 음악 재생 목록 클릭 시 나타나는 음악 플레이 화면
</pre>

<pre>
Main UI : 핸드폰의 내장 메모리(EXTERNAL_CONTENT_URI)에 접근해 listview에 올라가는 음악 파일들의 목록 화면은 다음과 같다.
</pre>

<div>
<img height = "350" src = "/uploads/361242053490907e7768379112a406ca/MP3_List_View.jpg">
</div>

<pre>
Music Play UI : 리스트에 있는 음악 목록을 클릭하면 이동하는 음악 재생 화면은 다음과 같다.
</pre>

<div>
<img height = "350" src = "/uploads/70dae7fd2662a725f98c620e909184e6/Mp3_Play_View.jpg">
</div>

# Music_Home.java
<pre>
※ m_list : 음악 정보들(Title, Artist)를 저장하는 가변 배열 

※ hasPermission : 내장 메모리에 접근 권한을 허용하는 boolean 변수

※ music_info : 가져올 컬럼의 목록

※ cursor : 내장 메모리에 query를 이용해 데이터를 조회하는 contentResolver를 통해 MediaStore.Audio.Media.EXTERNAL_CONTENT_URI 경로를 통해 음악 
            데이터를 가져와 한 행씩 참조할 수 있게 해주는 인터페이스

※ sqLiteDatabase : 음악의 플레이 리스트를 데이터베이스에 연결할 데이터베이스 객체

※ list : listview를 참조하는 리스트뷰. 이 리스트를 클릭하면 intent를 통해 음악을 재생하는 Music_Player 클래스로 이동

※ Myadapter : 화면에 보여지는 listview와 해당 listview에 올릴 데이터를 연결하는 중간 객체

※ Optiontionmenu : 옵션 메뉴
</pre>

# Music_Play.java
<pre>
title, play, pause, previous, next, shuffle, repeat, repeat_one, seekbar을 findViewById로 얻어온다.
position과 m_list는 Music_Home에서 Intent로 넘겨받은 getIntExtra와 getSerializableExtra를 통해 얻어온다.

seekBar는 getDuration 메소드를 통해 현 음악의 최대 플레이 타임을 seekBar의 길이만큼 지정한다.

※ onProgressChanged : 현재 드래그를 하고 있다는 것이 사실[if(fromUser)]이라면 그 때의 seekBar의 현 위치를 알아온다. 

※ onStopTrackingTouch : 드래그를 멈출 때 현재 멈춰진 seekBar의 위치를 찾아낸 후 시작 버튼이 사라진다면(음악이 재생중이라면) 음악을 그 지점부터 플레이한다. 

※ Music_Present : Intent로 전달받은 음악 정보가 담긴 리스트 m_list의 현 위치(position)을 인자로 받는 함수이다. 

※ Images : music.xml에서 선언한 모든 버튼들을 저장하는 배열이다. 

setDataSource를 이용해 m_list 음악 목록의 경로(getPath())를 알아낸다.
Runnable 인터페이스를 이용해 Thread를 생성하고 run 추상 메소드에서 음악이 플레이되는 동안 1초 간격으로 setProgress 메소드로 현 progress를 바꾸어 seekBar을 가게 한다.
</pre>

# Music_List.java
<pre>
음악들의 정보들이 커서를 통해 저장되는 클래스
</pre>

# List_View.java
<pre>
Music_List에 들어간 음악 정보들을 화면에 띄워주는 클래스
</pre>

# Direc, Sd.java
<pre>
ndk를 통해 핸드폰의 해당 경로로 들어갈 수 있게 해주는 클래스
</pre>

<div>
<img height = "350" src = "/uploads/8646f4d4bf8bac5dd47cf96d5ac66c8d/Screenshot_20191211-010036_C_Test.jpg">
</div>