package com.example.mp3_app;

import android.content.Context;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.widget.*;

public class List_View extends LinearLayout {
    ImageView IV;
    TextView TV;
    TextView TV_2;

    public List_View(Context context) {
        super(context);
        init(context);
    }

    public List_View(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(context);
    }

    private void init(Context context) {
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        inflater.inflate(R.layout.list,this,true);
        IV = findViewById(R.id.album);
        TV = findViewById(R.id.title);
        TV_2 = findViewById(R.id.artist);
    }
    public void setAlbum(int album){
        IV.setImageResource(album);
    }
    public void setTitle(String title){
        TV.setText(title);
    }
    public void setArtist(String artist){
        TV_2.setText(artist);
    }
}