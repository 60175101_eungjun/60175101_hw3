package com.example.c_test;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.provider.MediaStore;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.ListView;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import java.util.ArrayList;

public class Music_Home extends AppCompatActivity {
    private static final int REQUEST_WRITE_STORAGE = 112;
    ArrayList<Music_List> m_list = new ArrayList<>();
    SQLiteDatabase sqLiteDatabase;
    private final String DB = "Database";
    private final String table = "Tabler";
    Direc showDirec = new Direc();
    Sd showSd = new Sd();

    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.main, menu);
        return true;
    }
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main);

        boolean hasPermission = (ContextCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED);
        if (!hasPermission) {
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, REQUEST_WRITE_STORAGE);
        }

        String music_Info [] = {MediaStore.Audio.Media.TITLE, MediaStore.Audio.Media.ARTIST, MediaStore.Audio.Media.DATA};
        Cursor cursor = getContentResolver().query(MediaStore.Audio.Media.EXTERNAL_CONTENT_URI, music_Info, null, null, null);
        if(cursor != null && cursor.moveToFirst()) {
            while (cursor.moveToNext()) {
                try {
                    Music_List music_list = new Music_List();
                    music_list.setTitle(cursor.getString(cursor.getColumnIndex(MediaStore.Audio.Media.TITLE)));
                    music_list.setArtist(cursor.getString(cursor.getColumnIndex(MediaStore.Audio.Media.ARTIST)));
                    music_list.setPath(cursor.getString(cursor.getColumnIndex(MediaStore.Audio.Media.DATA)));
                    m_list.add(music_list);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
            cursor.close();
        }

        try{
            sqLiteDatabase = this.openOrCreateDatabase(DB, MODE_PRIVATE, null);
            sqLiteDatabase.execSQL("CREATE TABLE IF NOT EXISTS " + table + " (title varchar(50), artist varchar(20), path varchar(40));");
            for (int i = 0; i < m_list.size(); i++) {
                sqLiteDatabase.execSQL("INSERT INTO " + table + " (title) Values ('" + m_list.get(i) + "');");
            }
        }catch (Exception e){
            e.printStackTrace();
        }

        ListView list = findViewById(R.id.listview);
        MyAdapter adapter = new MyAdapter(getApplicationContext(), m_list);
        list.setAdapter(adapter);

        list.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            public void onItemClick(AdapterView<?> parent, View vClicked, int position, long id) {
                Intent intent = new Intent(Music_Home.this, Music_Play.class);
                intent.putExtra("position", position);
                intent.putExtra("m_list", m_list);
                startActivity(intent);
            }
        });
    }

    public class MyAdapter extends BaseAdapter {
        Context applicationContext;
        ArrayList<Music_List> m_list;
        public MyAdapter(Context applicationContext, ArrayList<Music_List> m_list) {
            this.applicationContext = applicationContext;
            this.m_list = m_list;
        }
        public int getCount() {
            return m_list.size();
        }
        public Object getItem(int position) {
            return m_list.get(position);
        }
        public long getItemId(int position) {
            return position;
        }
        public View getView(int position, View convertView, ViewGroup parent) {
            List_View view = new List_View(getApplicationContext());
            Music_List music_list = m_list.get(position);
            Cursor cursor = sqLiteDatabase.rawQuery("SELECT * FROM " + table, null);
            if (cursor != null && cursor.moveToFirst()) {
                while (cursor.moveToNext()) {
                    view.setTitle(cursor.getString(0));
                    view.setArtist(cursor.getString(1));
                }
                cursor.close();
            }
            view.setTitle(music_list.getTitle());
            view.setArtist(music_list.getArtist());
            return view;
        }
    }

    public boolean onOptionsItemSelected(MenuItem item){
        switch (item.getItemId()){
            case R.id.Music :
                break;
            case R.id.Direcory :
                showDirec.ShowDirec();
                break;
            case R.id.Sd :
                showSd.ShowSd();
                break;
        }
        return super.onOptionsItemSelected(item);
    }
}