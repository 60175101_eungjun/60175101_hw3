package com.example.mp3_app;

import androidx.appcompat.app.AppCompatActivity;
import android.media.MediaPlayer;
import android.content.Intent;
import java.util.ArrayList;
import android.os.Bundle;
import android.view.View;
import android.widget.*;

public class Music_Play extends AppCompatActivity {
    ImageView play, pause, previous, next, shuffle, repeat, repeat_one;
    MediaPlayer mediaPlayer = new MediaPlayer();
    ArrayList<Music_List> m_list;
    SeekBar seekBar;
    TextView title;
    int position;
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.music);

        title = findViewById(R.id.title);
        play = findViewById(R.id.play);
        pause = findViewById(R.id.pause);
        previous = findViewById(R.id.previous);
        next = findViewById(R.id.next);
        shuffle = findViewById(R.id.shuffle);
        repeat = findViewById(R.id.repeat);
        repeat_one = findViewById(R.id.repeat_one);
        seekBar = findViewById(R.id.seekbar);
        Intent intent = getIntent();
        position = intent.getIntExtra("position", 0);
        m_list = (ArrayList<Music_List>)intent.getSerializableExtra("m_list");
        Music_Present(m_list.get(position));

        seekBar.setMax(mediaPlayer.getDuration());
        seekBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser){
                if (fromUser) {
                    mediaPlayer.seekTo(progress);
                }
            }
            public void onStartTrackingTouch(SeekBar seekBar) {
            }
            public void onStopTrackingTouch(SeekBar seekBar) {
                mediaPlayer.seekTo(seekBar.getProgress());
                if(seekBar.getProgress() > 0 && play.getVisibility() == View.GONE){
                    mediaPlayer.start();
                }
            }
        });
    }

    public void Music_Present(final Music_List m_list) {
        ImageView Images[] = {play, pause, previous, next, shuffle, repeat, repeat_one};
        try{
            mediaPlayer.setDataSource(m_list.getPath()); // Get media path
            mediaPlayer.prepare();
            if(mediaPlayer.isPlaying()){
                mediaPlayer.pause();
            }
            mediaPlayer.start();
            new Thread(new Runnable() {
                public void run() {
                    while (mediaPlayer.isPlaying()) {
                        try {
                            Thread.sleep(1000);
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                        seekBar.setProgress(mediaPlayer.getCurrentPosition());
                    }
                }
            }).start();
            for (int i = 0; i < Images.length; i++) {
                Images[i].setOnClickListener(new View.OnClickListener() {
                    public void onClick(View v) {
                        if (v.getId() == R.id.play) {
                            play.setVisibility(View.GONE);
                            pause.setVisibility(View.VISIBLE);
                            play.setImageResource(R.drawable.ic_pause_circle_filled_black_24dp);
                            mediaPlayer.seekTo(mediaPlayer.getCurrentPosition());
                            mediaPlayer.start();
                            new Thread(new Runnable() {
                                public void run() {
                                    while (mediaPlayer.isPlaying()) {
                                        try {Thread.sleep(1000);} catch (Exception e) {e.printStackTrace();}
                                        seekBar.setProgress(mediaPlayer.getCurrentPosition());
                                    }
                                }
                            }).start();
                        } else if (v.getId() == R.id.pause) {
                            pause.setVisibility(View.GONE);
                            play.setVisibility(View.VISIBLE);
                            play.setImageResource(R.drawable.ic_play_circle_filled_black_24dp);
                            mediaPlayer.seekTo(mediaPlayer.getCurrentPosition());
                            mediaPlayer.pause();
                        } else if (v.getId() == R.id.previous) {
                            seekBar.setProgress(0);
                            mediaPlayer.seekTo(position--);
                            mediaPlayer.start();
                        } else if (v.getId() == R.id.next) {
                            seekBar.setProgress(0);
                            mediaPlayer.seekTo(position++);
                            mediaPlayer.start();
                        } else if (v.getId() == R.id.repeat) {
                            seekBar.setProgress(0);
                            repeat.setVisibility(View.GONE);
                            repeat_one.setVisibility(View.VISIBLE);
                            repeat.setImageResource(R.drawable.ic_repeat_one_black_24dp);
                            mediaPlayer.setLooping(true);
                        } else if (v.getId() == R.id.repeat_one) {
                            seekBar.setProgress(0);
                            repeat_one.setVisibility(View.GONE);
                            repeat.setVisibility(View.VISIBLE);
                            repeat.setImageResource(R.drawable.ic_repeat_black_24dp);
                            mediaPlayer.setLooping(false);
                        } else if (v.getId() == R.id.shuffle) {

                        } else if (mediaPlayer == null && !mediaPlayer.isPlaying()) {
                            mediaPlayer.start();
                            mediaPlayer.seekTo(mediaPlayer.getCurrentPosition());
                        }
                    }
                });
            }
        }catch (Exception e){
            e.printStackTrace();
        }
    }
}